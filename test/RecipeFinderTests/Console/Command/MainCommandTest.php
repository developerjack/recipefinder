<?php


/**
 *  RecipeFinderTests\Console\Command\MainCommandTest  class
 *
 * PHP version 5.3
 *
 * @category tests
 * @package  RecipeFinder
 * @author   Jack Skinner <sydnerdrage@gmail.com>
 *
 */

namespace RecipeFinderTests\Console\Command;

use Symfony\Bundle\FrameworkBundle\Client;
use Symfony\Component\Console\Tester\CommandTester;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Console\Application;

use RecipeFinder\Console\Command\MainCommand;

/**
 * MainCommandTest class
 *
 *
 *
 * @package  RecipeFinder
 * @author   Jack Skinner <sydnerdrage@gmail.com>
 * @license  MIT http://opensource.org/licenses/MIT
 * @link     https://bitbucket.org/sydnerdrage/recipefinder
 */

class MainCommandTest extends WebTestCase
{
    public function testSpecificationData()
    {
        $result = $this->runApplicationWith(array(
            'fridge' => 'data/fridge.csv',
            'recipes' => 'data/recipes.json'
        ));

        // regex to ignore new lines from the application
        $this->assertRegExp('/You should cook grilled cheese on toast/', $result);
    }

    public function testWithSomeoneElsesFridge()
    {
        $result = $this->runApplicationWith(array(
                'fridge' => 'data/someone_elses_fridge.csv',
                'recipes' => 'data/recipes.json'
            ));

        // regex to ignore new lines from the application
        $this->assertRegExp('/Order Takeout/', $result);
    }

    protected function runApplicationWith($config) {
        $application = new Application();
        $application->add(new MainCommand());

        $command = $application->find('run');
        $commandTester = new CommandTester($command);
        $commandTester->execute(
           array_merge(
               array( 'command' => $command->getName()),
               $config
           )
        );

        return $commandTester->getDisplay();
    }
}

