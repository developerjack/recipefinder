# Recipe Finder

Recipe Finder is a simple console application to help you decide what to eat for dinner.  Given your fridge and a list of cooking competencies (ok...  recipes) the recipe finder will choose the recipe for you.

## Usage
Running Recipe Finder is simple.  Simple invoke index.php with your fridge (csv) and recipe list (json) as arguments.

```bash
php rf.php run fridge.csv recipes.json
```

## Your Fridge
Recipe Finder requires your fridge to be provided as a .csv file in the format 'item, amount, unit, expiry'.  See below:

> bread,10,slices,25/12/2014

### Valid Units
A fridge CSV currently supports four units of measurement.

 - "of"  (for individual items e.g. eggs)
 - "grams"
 - "ml"  (millilitres)
 - "slices"

Expiry dates MUST be given in the format dd/mm/yyyy.


## Recipies
Recipies can be provided to Recipe Finder through a JSON array.  A sample can be found below:

```json

[
    {
        "name": "grilled cheese on toast",
        "ingredients": [
            {
                "item": "bread",
                "amount": "2",
                "unit": "slices"
            },
            {
                "item": "cheese",
                "amount": "2",
                "unit": "slices"
            }
        ]
    }
]
```

### Valid Units
The recipe list uses the same limits as the fridge ([details above](#valid-units)).

## Roadmap
 - Package into a .phar for distribution and execution
 - Replace factory instantiation/configuration with Entity hydration. See \Zend\Stdlib\Hydrator for possible
 implementations.


