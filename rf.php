#!/usr/bin/env php
<?php
require 'vendor/autoload.php';

use RecipeFinder\Console\Command\MainCommand;
use Symfony\Component\Console\Application;

$application = new Application();
$application->add(new MainCommand);
$application->run();