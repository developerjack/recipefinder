<?php


/**
 *  RecipeFinder\Console\Command\MainCommand  class
 *
 * PHP version 5.3
 *
 * @package  RecipeFinder
 * @author   Jack Skinner <sydnerdrage@gmail.com>
 *
 */

namespace RecipeFinder\Console\Command;


use Cookbook\Cookbook;
use Cookbook\Decision\DecisionManager;
use Cookbook\Decision\Strategy\ExpiryDateStrategy;
use Cookbook\Model\Fridge;
use RecipeFinder\Entity\Factory\IngredientFactory;
use RecipeFinder\Entity\Factory\RecipeFactory;
use RecipeFinder\Exception\FileNotFoundException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

 /**
 * MainCommand class
 *
 * 
 *
 * @package  RecipeFinder
 * @author   Jack Skinner <sydnerdrage@gmail.com>
 * @license  MIT http://opensource.org/licenses/MIT
 * @link     https://bitbucket.org/sydnerdrage/recipefinder
 */


class MainCommand extends Command
{
    protected function configure()
    {
        $this
            ->setName('run')
            ->setDescription('Run the RecipeFinder application.')
            ->addArgument(
                'fridge',
                InputArgument::REQUIRED,
                '.csv file containing your fridge\'s items.'
            )
            ->addArgument(
                'recipes',
                InputArgument::REQUIRED,
                '.json file containing your recipes.'
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $fridgeCsvPath = $input->getArgument('fridge');
        $recipesJsonPath = $input->getArgument('recipes');

        // Load the data out of the file.
        $fridgeData = $this->loadFridgeCsv($fridgeCsvPath);

        if(is_null($fridgeData)) {
            throw new \LogicException("The fridge was not loaded or was empty.");
        }
        $ingredients = $this->createFridgeFromData($fridgeData);


        // Load the data out of the file.
        $recipeData = $this->loadRecipesPath($recipesJsonPath);
        if(is_null($recipeData)) {
            throw new \LogicException("Recipes were not loaded or was empty.");
        }
        // Transform the data into usable entities
        $recipes = $this->createRecipesFromData($recipeData);


        /**
         * Run the Cookbook class.
         */
        $cookbook = new Cookbook();
        $cookbook->setIngredients($ingredients);
        $cookbook->setRecipes($recipes);

        $fridge = new Fridge();
        $fridge->setIngredients($ingredients);
        $cookbook->setFridge($fridge);

        $decisionManager = new DecisionManager();
        $decisionManager->addStrategy('expiry-date', new ExpiryDateStrategy());
        $cookbook->setDecisionManager($decisionManager);

        $recipe = $cookbook->chooseOptimalRecipe();

        if(is_null($recipe)) {
            $output->writeln("Order Takeout");
            return;
        }
        $answer = $recipe->getName();

        $output->writeln("You should cook $answer.");
    }

    protected function loadFridgeCsv($path) {
        $fields = array (
            'item',
            'amount',
            'units',
            'use-by',
        );

        if(!file_exists($path) || !is_readable($path)) {
            throw new FileNotFoundException("Fridge CSV file does not exist or is not accessible.");
        }

        $csv = array();

        $f = fopen($path, 'r');
        while (($line = fgetcsv($f)) !== FALSE) {
            $csv[] = array_combine($fields, $line);
        }

        return $csv;

    }

    protected function loadRecipesPath($path) {

        if(!file_exists($path) || !is_readable($path)) {
            throw new FileNotFoundException("Recipes JSON file does not exist or is not accessible.");
        }

        $json = file_get_contents($path);

        $json = json_decode($json, true); // true == as assoc array

        return $json;
    }

    public function createFridgeFromData( array $data ) {
        $ingredients = array ();
        foreach($data as $ingredientData) {
            $ingredients[] = IngredientFactory::create($ingredientData);
        }
        return $ingredients;
    }

    public function createRecipesFromData( array $data ) {
        $recipes = array();
        foreach($data as $recipeData) {
            $recipes[] = RecipeFactory::create($recipeData);
        }
        return $recipes;
    }


}
 