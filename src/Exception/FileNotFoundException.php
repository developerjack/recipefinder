<?php


/**
 *  RecipeFinder\Exception\FileNotFoundException  class
 *
 * PHP version 5.3
 *
 * @package  RecipeFinder
 * @author   Jack Skinner <sydnerdrage@gmail.com>
 *
 */

namespace RecipeFinder\Exception;

 /**
 * FileNotFoundException class
 *
 * 
 *
 * @package  RecipeFinder
 * @author   Jack Skinner <sydnerdrage@gmail.com>
 * @license  MIT http://opensource.org/licenses/MIT
 * @link     https://bitbucket.org/sydnerdrage/recipefinder
 */

class FileNotFoundException extends \Exception
{

}
 