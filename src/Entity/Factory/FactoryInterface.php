<?php

/**
 *  RecipeFinder\Entity\Factory\FactoryInterface  class
 *
 * PHP version 5.3
 *
 * @package  RecipeFinder
 * @author   Jack Skinner <sydnerdrage@gmail.com>
 *
 */

namespace RecipeFinder\Entity\Factory;

 /**
 * FactoryInterface class
 *
 * 
 *
 * @package  RecipeFinder
 * @author   Jack Skinner <sydnerdrage@gmail.com>
 * @license  MIT http://opensource.org/licenses/MIT
 * @link     https://bitbucket.org/sydnerdrage/recipefinder
 */

interface FactoryInterface
{
    public static function create($data);

}
 