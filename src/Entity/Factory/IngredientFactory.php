<?php
namespace RecipeFinder\Entity\Factory;

use Cookbook\Entity\Ingredient;
use RecipeFinder\Entity\Factory\FactoryInterface;

/**
 *  RecipeFinder\Entity\IngredientFactory class
 *
 * PHP version 5.3
 *
 * @package  RecipeFinder
 * @author   Jack Skinner <sydnerdrage@gmail.com>
 *
 */

 /**
 * IngredientFactory class
 *
 * 
 *
 * @package  RecipeFinder
 * @author   Jack Skinner <sydnerdrage@gmail.com>
 * @license  MIT http://opensource.org/licenses/MIT
 * @link     https://bitbucket.org/sydnerdrage/recipefinder
 */

class IngredientFactory implements FactoryInterface
{

    public static function create($config) {

        $ingredient = new Ingredient();
        $ingredient->setItem($config['item']);
        $ingredient->setAmount($config['amount']);
        $ingredient->setUnit($config['units']);
        $ingredient->setUseBy( \DateTime::createFromFormat('d/m/Y', $config['use-by']));

        return $ingredient;
    }

}
 