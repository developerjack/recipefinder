<?php
namespace RecipeFinder\Entity\Factory;

use Cookbook\Entity\Food;
use Cookbook\Entity\Recipe;
use RecipeFinder\Entity\Factory\FactoryInterface;

/**
 *  RecipeFinder\Entity\Factory\Recipe class
 *
 * PHP version 5.3
 *
 * @package  RecipeFinder
 * @author   Jack Skinner <sydnerdrage@gmail.com>
 *
 */

 /**
 * RecipeFactory class
 *
 * 
 *
 * @package  RecipeFinder
 * @author   Jack Skinner <sydnerdrage@gmail.com>
 * @license  MIT http://opensource.org/licenses/MIT
 * @link     https://bitbucket.org/sydnerdrage/recipefinder
 */

class RecipeFactory implements FactoryInterface
{

    public static function create($config) {

        $recipe = new Recipe();
        $recipe->setName($config['name']);

        $items = array();
        foreach($config['ingredients'] as $i) {
            $item = new Food();
            $item->setItem($i['item']);
            $item->setAmount($i['amount']);
            $item->setUnit($i['unit']);
            $items[] = $item;
        }
        $recipe->setIngredients($items);

        return $recipe;
    }

}
 